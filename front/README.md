# Front

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.3.6.

## Development server

Run `npm install` to install de dependences.

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Descripción proyecto

Gráfico Multi Line de Chart Js 3.7.1 configurable desde la constante inicial de configuracion "NewChartConfig". Contiene escalas de 7 valores para los ejes "y" e "y1" calculadas sobre la división de los máximos acorde a la cantidad de ejes que se requieran y 12 valores (meses) en eje x. Los valores dummy son randoms aproximados creados.
Al hacer hover sobre los puntos se ejecuta el evento onHover que dispara un evento "mousemove", se anula el mes indicado y se pinta uno nuevo con el color correspondiente al dataset, lo mismo para el eje "y" pero en este caso sin remarcar el axis porque los valores no son exactos entonces se decidió mostrarlo por encima del punto.

PD: El onHover tiene una especie de "flasheo" que no se terminó de solucionar.

<!-- ## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. -->

<!-- ## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io). -->

<!-- ## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities. -->

<!-- ## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page. -->
