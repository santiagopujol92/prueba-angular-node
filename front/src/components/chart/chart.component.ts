import { Component, OnInit } from '@angular/core';
import {
  Chart,
  ArcElement,
  LineElement,
  BarElement,
  PointElement,
  BarController,
  BubbleController,
  DoughnutController,
  LineController,
  PieController,
  PolarAreaController,
  RadarController,
  ScatterController,
  CategoryScale,
  LinearScale,
  LogarithmicScale,
  RadialLinearScale,
  TimeScale,
  TimeSeriesScale,
  Decimation,
  Filler,
  Legend,
  Title,
  Tooltip,
  Ticks
} from 'node_modules/chart.js'

Chart.register(
  ArcElement,
  LineElement,
  BarElement,
  PointElement,
  BarController,
  BubbleController,
  DoughnutController,
  LineController,
  PieController,
  PolarAreaController,
  RadarController,
  ScatterController,
  CategoryScale,
  LinearScale,
  LogarithmicScale,
  RadialLinearScale,
  TimeScale,
  TimeSeriesScale,
  Decimation,
  Filler,
  Legend,
  Title,
  Tooltip
);

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})

export class ChartComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    const NewChartConfig = {
      TITLE_DATASET1: "Line Dataset 1",
      TITLE_DATASET2: "Line Dataset 2",
      DATASET1: [292.65, 292.68, 292.82, 292.54, 292.54, 292.41, 292.41, 292.68, 292.27, 292.41, 292.27, 292.38],
      DATASET2: [292.16, 292.27, 292.27, 292.27, 292.41, 292.52, 292.53, 292.68, 292.82, 292.95, 292.98, 292.95],
      COLOR_POINT_DATASET1: "#FFC127",
      COLOR_PRIM_DATASET1: "#C29116",
      COLOR_SEC_DATASET1: "#785B13",
      COLOR_POINT_DATASET2: "#5A5AF7",
      COLOR_PRIM_DATASET2: "#4A4AC4",
      COLOR_SEC_DATASET2: "#323281",
      DATA_MAX_Y0: 293.00,
      DATA_MIN_Y0: 292.00,
      DATA_MAX_Y1: 30,
      DATA_MIN_Y1: 5,
      DATA_X: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
      TOTAL_LINES_EJE_Y: 6,
    }

    var hoverPointIndex = -1;

    function drawNewLabels(e: any, item: any) {
      if (item.length > 0) {
        let i = item[0].index;
        let ctx = e.chart.$context.chart.ctx;
        let label = e.chart.config._config.data.labels[i]
        let datasets = e.chart.config._config.data.datasets
        let datasetSelected = e.chart.tooltip.dataPoints ? e.chart.tooltip.dataPoints[0].dataset.order : null;
        let colorDataset = datasetSelected == 0 ? NewChartConfig.COLOR_PRIM_DATASET1 : NewChartConfig.COLOR_PRIM_DATASET2;

        if (datasetSelected != null) {

          // Painting in X Bottom (months) by dataset point 
          ctx.fillStyle = colorDataset;
          ctx.font = "bold";
          ctx.lineWidth = 5;
          ctx.save()
          ctx.fillText(label, item[0].element.x - 10.17, 726);

          // Painting in Y left by dataset point
          ctx.fillStyle = colorDataset;
          ctx.font = "bold";
          ctx.lineWidth = 5;
          ctx.save()
          ctx.fillText(datasets[datasetSelected].data[i] + " K", item[0].element.x - 22, item[0].element.y - 32);

          // Painting circle in the center
          ctx.fillStyle = datasetSelected == 0 ? NewChartConfig.COLOR_POINT_DATASET1 : NewChartConfig.COLOR_POINT_DATASET2;
          ctx.font = "bold"
          ctx.lineWidth = 5;
          ctx.save()
          ctx.beginPath();
          ctx.arc(item[0].element.x, item[0].element.y, 3, (Math.PI / 180) * 360, false)
          ctx.fill()
          ctx.closePath()

          myChart.update()
        }
      }
    }

    var myChart = new Chart("chartCanvas", {
      type: 'line',
      data: {
        datasets: [{
          label: NewChartConfig.TITLE_DATASET1,
          data: NewChartConfig.DATASET1,
          type: 'line',
          backgroundColor: NewChartConfig.COLOR_PRIM_DATASET1,
          pointBorderColor: NewChartConfig.COLOR_PRIM_DATASET1,
          pointBorderWidth: 2,
          order: 0,
          tension: 0.4,
          borderColor: NewChartConfig.COLOR_PRIM_DATASET1,
          borderWidth: 1.8,
          pointHoverRadius: 10,
          pointHoverBorderWidth: 6,
          pointHoverBackgroundColor: NewChartConfig.COLOR_PRIM_DATASET1,
          pointHoverBorderColor: NewChartConfig.COLOR_SEC_DATASET1,
        }, {
          label: NewChartConfig.TITLE_DATASET2,
          data: NewChartConfig.DATASET2,
          backgroundColor: NewChartConfig.COLOR_PRIM_DATASET2,
          pointBorderColor: NewChartConfig.COLOR_PRIM_DATASET2,
          pointBorderWidth: 2,
          order: 1,
          tension: 0.4,
          borderColor: NewChartConfig.COLOR_PRIM_DATASET2,
          borderWidth: 1.8,
          pointHoverRadius: 10,
          pointHoverBorderWidth: 6,
          pointHoverBackgroundColor: NewChartConfig.COLOR_PRIM_DATASET2,
          pointHoverBorderColor: NewChartConfig.COLOR_SEC_DATASET2,
        }],
        labels: NewChartConfig.DATA_X,
      },
      options: {
        plugins: {
          title: {
            display: false,
          },
          legend: {
            display: false
          },
        },
        responsive: true,
        layout: {
          padding: 10
        },
        interaction: {
          intersect: false,
        },
        scales: {
          x: {
            labels: NewChartConfig.DATA_X,
            grid: {
              display: false,
            },
            ticks: {
              color: "white",
              callback: function (value, index) {
                let label = this.getLabelForValue(index);
                if (index == hoverPointIndex) {
                  return '';
                }
                return label
              },
              padding: 10,
            },
          },
          y: {
            beginAtZero: false,
            ticks: {
              callback: function (value) {
                return Number(value).toFixed(2) + " K"
              },
              padding: 20,
              color: "white",
              stepSize: (NewChartConfig.DATA_MAX_Y0 - NewChartConfig.DATA_MIN_Y0) / NewChartConfig.TOTAL_LINES_EJE_Y,

            },
            min: NewChartConfig.DATA_MIN_Y0,
            max: NewChartConfig.DATA_MAX_Y0,
            type: "linear",
            grid: {
              borderWidth: 0,
              drawTicks: false,
              lineWidth: 0.4,
              borderDash: [1, 3],
              color: '#6F6C6B',
              borderColor: '#6F6C6B',
              tickColor: '#6F6C6B',
            },
          },
          y1: {
            position: 'right',
            type: 'linear',
            beginAtZero: true,
            ticks: {
              callback: function (value, index) {
                return Number(value).toFixed(2)
              },
              padding: 20,
              color: "#6A6A6A",
              stepSize: (NewChartConfig.DATA_MAX_Y1 - NewChartConfig.DATA_MIN_Y1) / NewChartConfig.TOTAL_LINES_EJE_Y,
            },
            min: NewChartConfig.DATA_MIN_Y1,
            max: NewChartConfig.DATA_MAX_Y1,
            grid: {
              drawOnChartArea: false,
              borderWidth: 0,
              drawTicks: false,
              lineWidth: 0.4,
              borderDash: [1, 3],
              color: '#6F6C6B',
              borderColor: '#6F6C6B',
              tickColor: '#6F6C6B',
            },
          }
        },
        hover: {
          mode: 'nearest',
          intersect: false,
        },
        onHover: function (e, item) {
          if (item.length == 1) {
            hoverPointIndex = item[0].index
            drawNewLabels(e, item);
          }
        },
      }
    });

  }
}