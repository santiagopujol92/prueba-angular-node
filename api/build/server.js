"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
/** src/server.ts */
const http_1 = __importDefault(require("http"));
const app_1 = __importDefault(require("./app"));
/** Server */
const httpServer = http_1.default.createServer(app_1.default);
const PORT = (_a = process.env.PORT) !== null && _a !== void 0 ? _a : 6060;
httpServer.listen(PORT, () => console.log(`The server is running on port ${PORT}`));
