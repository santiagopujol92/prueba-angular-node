"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/** src/routes/mutation.ts */
const express_1 = __importDefault(require("express"));
const mutation_1 = __importDefault(require("../controllers/mutation"));
const router = express_1.default.Router();
router.post('/hasMutation', mutation_1.default.hasMutation);
router.get('/stats', mutation_1.default.stats);
router.get('/', mutation_1.default.index);
exports.default = router;
