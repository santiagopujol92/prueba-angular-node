"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getDocsByRef = exports.addDocByRef = void 0;
/** src/services/firebase.ts */
const app_1 = require("firebase/app");
const firestore_1 = require("firebase/firestore");
const firebaseConfig = {
    apiKey: "AIzaSyCHmzCwGCo_4u50kj93WWdB4N-9c1MIgYU",
    authDomain: "prueba-api-f8fb7.firebaseapp.com",
    projectId: "prueba-api-f8fb7",
    storageBucket: "prueba-api-f8fb7.appspot.com",
    messagingSenderId: "545151736765",
    appId: "1:545151736765:web:19228fbf5f4572137250d4"
};
(0, app_1.initializeApp)(firebaseConfig);
const db = (0, firestore_1.getFirestore)();
// Add doc by ref to firebase
function addDocByRef(colRef, data) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield (0, firestore_1.addDoc)((0, firestore_1.collection)(db, colRef), data);
        }
        catch (error) {
            console.log('error adding ref', error);
        }
    });
}
exports.addDocByRef = addDocByRef;
// Get all docs by ref to firebase
function getDocsByRef(colRef) {
    return __awaiter(this, void 0, void 0, function* () {
        const col = (0, firestore_1.collection)(db, colRef);
        const snapshot = yield (0, firestore_1.getDocs)(col);
        return snapshot.docs.map(doc => doc.data());
    });
}
exports.getDocsByRef = getDocsByRef;
