/** src/app.ts */
import express from 'express';
import morgan from 'morgan';
import routesMutation from './routes/mutation';

const router = express();

/** Logging */
router.use(morgan('dev'));
/** Parse the request */
router.use(express.urlencoded({ extended: false }));
/** Takes care of JSON data */
router.use(express.json());

/** RULES OF OUR API */
router.use((req, res, next) => {
    // set the CORS policy
    res.header('Access-Control-Allow-Origin', '*');
    // set the CORS headers
    res.header('Access-Control-Allow-Headers', 'origin, X-Requested-With,Content-Type,Accept, Authorization');
    // set the CORS method headers
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'GET PATCH DELETE POST');
        return res.status(200).json({});
    }
    next();
});


/** Routes */
router.use('/mutation', routesMutation);

/** Error handling */
router.use((req, res, next) => {
    if (req.url === "/") {
        return res.status(200).send("Welcome Node JS Api")
    }

    const error = new Error('endpoint not found');
    return res.status(404).json({
        message: error.message
    });
});

export default router
