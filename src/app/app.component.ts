import { Component, ViewChild, ElementRef, OnInit, ɵConsole } from '@angular/core';
import { ɵangular_packages_router_router_k } from '@angular/router';
import { Chart, ChartDataSets, ChartData, ChartOptions } from 'chart.js';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  @ViewChild('grafica', { static: true }) graficaREF: ElementRef;

  ngOnInit() {

    const myCanvas = document.getElementById('grafica') as HTMLCanvasElement

    // DATASET DUMMY CONSTANTS
    const DATASET1 = [292.67, 292.82, 292.54, 292.54, 292.95, 292.41, 292.68, 292.27, 292.41, 292.21, 292.37, 292.50];
    const DATASET2 = [21.43, 21.43, 26.78, 16.07, 16.07, 10.71, 10.71, 21.43, 5.36, 10.71, 5.36, 10];
    const DATASET1_MAX = 293;
    const DATASET1_MIN = Math.min(...DATASET1);
    const DATASET2_MAX = 30;
    const DATASET2_MIN = Math.min(...DATASET2);
    const TOTAL_LINES_BY_EJE = 6;
    const DATASET_EJEX = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dic']

    var hoverPointXIndex = -1;
    var hoverPointYIndex = -1;
    var hoverDatasetIndex = -1;
    var areaValueAxisY = DATASET1_MAX;

    // CHARTDATASET CONFIGURATION
    const datasets: ChartDataSets[] = [
      {
        label: 'Fan page',
        fill: false,
        data: DATASET1,
        backgroundColor: '#4C4CD8',
        borderColor: '#4C4CD8',
        borderWidth: 1,
        yAxisID: 'Fan page',
        pointHoverRadius: 7,
        pointHoverBackgroundColor: '#4C4CD8',
        pointHoverBorderColor: '#4c4cd8a1',
        pointHoverBorderWidth: 10,
      },
      {
        label: 'Engaged users',
        fill: false,
        data: DATASET2,
        backgroundColor: '#F8CB1C',
        borderColor: '#F8CB1C',
        borderWidth: 1,
        yAxisID: 'Engaged users',
        pointHoverRadius: 7,
        pointHoverBackgroundColor: '#F8CB1C',
        pointHoverBorderColor: '#f8cb1c85',
        pointHoverBorderWidth: 10
      }
    ]

    // CHARTDATA
    const data: ChartData = {
      labels: DATASET_EJEX,
      datasets
    }

    // CHARTOPTIONS
    const options: ChartOptions = {
      responsive: false,
      legend: {
        display: false
      },
      hover: {
        mode: "x",
        intersect: false
      },
      scales: {
        xAxes: [
          {
            ticks: {
              fontColor: "#EBEBEB",
              callback: (value, index, ticks) => {
                if (index === hoverPointXIndex) {
                  return value.toString().toUpperCase()
                } else {
                  return value
                }
              },
              fontSize: 10,
              fontFamily: 'Work Sans',
              padding: 6
            },
            gridLines: {
              display: false,
            },
          },
        ],
        yAxes: [{
          id: 'Fan page',
          position: 'left',
          ticks: {
            fontColor: ['#EBEBEB'],
            padding: 15,
            fontSize: 10,
            fontFamily: 'Work Sans',
            callback: (value: string, index, values) => {
              const newValue = parseFloat(value)
              return newValue.toFixed(2) + ' K'
            },
            stepSize: (DATASET1_MAX - DATASET1_MIN) / TOTAL_LINES_BY_EJE,
          },
          gridLines: {
            borderDash: [1, 2],
            color: "#282828",
            drawBorder: false,
            tickMarkLength: 0,
          }
        },
        {
          id: 'Engaged users',
          position: 'right',
          ticks: {
            fontColor: '#B8B7B7',
            fontSize: 10,
            padding: 15,
            fontFamily: 'Work Sans',
            callback: (value: string, index, values) => {
              const newValue = parseFloat(value).toFixed(2)
              return newValue
            },
            stepSize: (DATASET2_MAX - DATASET2_MIN) / TOTAL_LINES_BY_EJE,
          },
          gridLines: {
            drawBorder: false,
            tickMarkLength: 0,
            color: 'transparent'
          }
        }]
      },
      tooltips: {
        enabled: false,
        mode: 'x',
        intersect: false
      },
      onHover: (e, item) => {
        drawShadowPoint(e, item)
        changeColorLabels(e, item)
        myChart.update({ duration: 0 });
      },
    }

    // INIT GRAPH
    const myChart = new Chart('grafica', {
      type: 'line',
      data,
      options,
    })

    function mousemoveHandler(mousemove) {
      const points = myChart.getElementsAtEventForMode(mousemove, 'nearest', {
        intersect: false
      }, true);

      if (points[0] != null) {
        const datasetIndex = points[0]._datasetIndex;
        const datapointIndex = points[0]._index;

        if (points[0]._datasetIndex != null && points[0]._index != null) {
          console.log(points[0])
          hoverDatasetIndex = datasetIndex;
          hoverPointXIndex = datapointIndex;
        }
      }
    }

    function drawShadowPoint(e, item) {
      // ACLARACION: Se genera una sombra en el medio del punto ya que no funcionaba el drawArc para dibujar un círculo 
      // por fuera del punto, con su color mas opaco, sin rellenar y con el border grueso se lograba
      if (item.length > 0) {
        const ctx = myChart.ctx;
        let stroke = ctx.stroke;
        ctx.stroke = function () {
          ctx.save()
          ctx.shadowColor = "#282828";
          ctx.shadowBlur = 10;
          ctx.shadowOffsetX = 0;
          ctx.shadowOffsetY = 0;
          stroke.apply(this, arguments)
          ctx.restore()
        }
      }
    }

    function changeColorLabels(e, item) {
      // ACLARACION: No se pudo lograr acceder al index de los axis para colorear el correspondiente
      // hoverPointXIndex contiene el index de X (axes de meses) para seleccionar el tick
      if (item.length > 0) {
        myChart.config.options.scales.xAxes[0].ticks.fontColor = hoverDatasetIndex == 0 ? "#4C4CD8" : "#F8CB1C"
        if (hoverDatasetIndex != -1) {
          myChart.config.options.scales.yAxes[hoverDatasetIndex].ticks.fontColor = hoverDatasetIndex == 0 ? "#4C4CD8" : "#F8CB1C"
        }
      } else {
        myChart.config.options.scales.xAxes[0].ticks.fontColor = "white"
        if (hoverDatasetIndex != -1) {
          myChart.config.options.scales.yAxes[hoverDatasetIndex].ticks.fontColor = hoverDatasetIndex == 0 ? "#EBEBEB" : "#B8B7B7"
        }
      }
    }

    myCanvas.onmousemove = mousemoveHandler
  }
}
