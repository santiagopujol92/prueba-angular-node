# Prueba

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.2.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


## Información adicional
Carpetas con proyectos:

* prueba-chartjs (prueba-angular-node en mi repo)
		Proyecto del repositorio con version Chart JS 2.9.4.
		En este caso se logró el objetivo de los 7 labels en los Axes, el círculo en el punto no se logro dibujarlo sino que se realizó una sombra en el centro dandole un estilo presentable, y el objetivo de colorear los ticks tampoco se logró completar en su totalidad por no poder obtener el índice del tick y su fuente para modificar. 

		Se aclara más detalles en documentación de codigo y en README.md.

* prueba-chartjs/front
		Proyecto angular 13 con Chart Js 3.7
		Se monto el proyecto de 0 a partir del análisis de las fotos que recibí en el email ya que no tuve el acceso al repositorio hasta el día lunes. Se trabajó en los objetivos, sin lograr la efectividad 100% de los mismos, pero se tomaron alternativas con fundamento ya que se probaron muchas soluciones, lo cual llevo a demorar un tiempo excedido para el problema de de colorear el tick del Eje Y y pintar el círculo con menor opacidad.

		Se aclara más detalles en documentación de código y en README.md.

* prueba-chartjs/api
		Proyecto Api Node JS v16 utilizando Express Firebase, Jest.

		Se aclara más detalles en documentación de código y en README.md 
